(() => {
  let vm = new Vue({
    el: '#wrapper',
    data() {
      return {
        checkedSum: 0,
        coeff: 180 / 100,
        angleCorrection: 90,
        animatedNumber: 0
      }
    },
    created() {
      $('#wrapper').show();

      //init value, bad practice
      $('.skill__checkbox input[type="checkbox"]').each((index, element) => {
        if (element.checked) {
          this.checkedSum += +element.value;
        }
      })
    },
    methods: {
      selectSkill(e) {
        let target = e.target;
        let value = +target.value;

        if (target.checked) {
          this.checkedSum += value;
        } else {
          this.checkedSum -= value;
        }
      }
    },
    computed: {
      skillMeterStyle() {
        let angle = this.checkedSum * this.coeff - this.angleCorrection;

        return {
          'transform': `rotate(${angle}deg)`
        }
      }
    },
    watch: {
      checkedSum(newValue, oldValue) {
        let vm = this;

        function animate() {
          if (TWEEN.update()) {
            requestAnimationFrame(animate);
          }
        }

        new TWEEN.Tween({tweeningNumber: oldValue})
        .easing(TWEEN.Easing.Quadratic.Out)
        .to({tweeningNumber: newValue}, 300)
        .onUpdate(function () {
          vm.animatedNumber = +(this.tweeningNumber * 50).toFixed(0)
        })
        .start();

        animate();
      }
    }
  });
})();
