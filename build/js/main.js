'use strict';

svg4everybody();

(function() {
	var iterate = function iterate(items, callback) {
		items.forEach(function(item) {
			var key = void 0;
			var value = void 0;

			if (typeof item === 'string') {
				key = item;
				value = item;
			} else {
				key = item[0];
				value = item[1];
			}

			callback(key, value);
		});
	};

	var check = function check(category, items) {
		iterate(items, function(key, value) {
			if (bowser[key]) {
				$(document.documentElement).addClass('is-' + category + '-' + value);
			}
		});
	};

	check('engine', ['webkit', 'blink', 'gecko', ['msie', 'ie'],
		['msedge', 'edge']
	]);

	check('device', ['mobile', 'tablet']);

	check('browser', ['chrome', 'firefox', ['msie', 'ie'],
		['msedge', 'edge'], 'safari', 'android', 'ios', 'opera', ['samsungBrowser', 'samsung'], 'phantom', 'blackberry', 'webos', 'silk', 'bada', 'tizen', 'seamonkey', 'sailfish', 'ucbrowser', 'qupzilla', 'vivaldi', 'sleipnir', 'kMeleon'
	]);

	check('os', ['mac', 'windows', 'windowsphone', 'linux', 'chromeos', 'android', 'ios', 'iphone', 'ipad', 'ipod', 'blackberry', 'firefoxos', 'webos', 'bada', 'tizen', 'sailfish']);
})();

/* eslint-disable no-unused-vars */

var $window = $(window);
var $document = $(document);
var $html = $(document.documentElement);
var $body = $(document.body);

(function() {
	var vm = new Vue({
		el: '#wrapper',
		data: function data() {
			return {
				checkedSum: 0,
				coeff: 180 / 100,
				angleCorrection: 90,
				animatedNumber: 0
			};
		},
		created: function created() {
			var _this = this;

			$('#wrapper').show();

			//init value, bad practice
			$('.skill__checkbox input[type="checkbox"]').each(function(index, element) {
				if (element.checked) {
					_this.checkedSum += +element.value;
				}
			});
		},

		methods: {
			selectSkill: function selectSkill(e) {
				var target = e.target;
				var value = +target.value;

				if (target.checked) {
					this.checkedSum += value;
				} else {
					this.checkedSum -= value;
				}
			}
		},
		computed: {
			skillMeterStyle: function skillMeterStyle() {
				var angle = this.checkedSum * this.coeff - this.angleCorrection;

				return {
					'transform': 'rotate(' + angle + 'deg)'
				};
			}
		},
		watch: {
			checkedSum: function checkedSum(newValue, oldValue) {
				var vm = this;

				function animate() {
					if (TWEEN.update()) {
						requestAnimationFrame(animate);
					}
				}

				new TWEEN.Tween({
					tweeningNumber: oldValue
				}).easing(TWEEN.Easing.Quadratic.Out).to({
					tweeningNumber: newValue
				}, 300).onUpdate(function() {
					vm.animatedNumber = +(this.tweeningNumber * 50).toFixed(0);
				}).start();

				animate();
			}
		}
	});
})();;
